function limitFunctionCallCount(cb, n) {

    function invokeCB(){
        for(let index = 0;index<n;index++){
            cb(index+1)
        }  
    }
    if (n<=0){
        return null
    }
    else{
        return invokeCB
    }

    // Should return a function that invokes `cb`.
    // The returned function should only allow `cb` to be invoked `n` times.
    // Returning null is acceptable if cb can't be returned
}

module.exports = limitFunctionCallCount;