function counterFactory(count) {
    let counter = count

    function methodIncre(){
        return counter+=10
    }
    function methodDecre(){
        return counter-=20
    }
    let obj = {
        "increment": methodIncre,
        "decrement": methodDecre
    }
    return obj
    // Return an object that has two methods called `increment` and `decrement`.
    // `increment` should increment a counter variable in closure scope and return it.
    // `decrement` should decrement the counter variable and return it.
}

module.exports = counterFactory;