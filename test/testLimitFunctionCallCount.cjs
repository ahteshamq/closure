let test = require("../limitFunctionCallCount.cjs")

function printCount(n){
    console.log(n)
}

let demo = test(printCount,5)
if (demo === null){
    console.log(null)
}
else{
    demo()
}